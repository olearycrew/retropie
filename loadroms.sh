retropie_path="/home/pi/RetroPie"
#retropie_path="/Users/brendan/repos/personal/fakehome"
usb_path="$pwd"

## functions
function log() {
    logger -p user.$1 -t usbmount-"$hook_name"-[$$] -- "$2"
}

function log_cmd() {
    local ret
    local error
    error="$("$@" 2>&1 >/dev/null)"
    ret=$?
    [[ "$ret" -ne 0 ]] && log err "$* - returned $ret - $error"
}

for D in `find ./roms -type d`
do
    echo "Syncing $D ..."
    rsync -rtu --exclude '._*' --max-delete=-1 "$D" "$retropie_path/"
    chown -R $user:$user "$retropie_path/$D"
done

echo "Restart emulation station"
echo "" > /tmp/es-restart && killall emulationstation